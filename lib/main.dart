import 'package:flutter/material.dart';
import 'utils/index.dart';
import 'components/product-screen-top-part.dart';
import 'components/product-screen-bottom-part.dart';


void main() => runApp(MaterialApp(
  home: SneakeShopAdidas(),
  debugShowCheckedModeBanner: false,
));

class SneakeShopAdidas extends StatefulWidget {
  SneakeShopAdidasState createState() => new SneakeShopAdidasState();
}

class SneakeShopAdidasState extends State<SneakeShopAdidas> {

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Color(0xFF696D77),
            Color(0xFF292C36)
          ],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          tileMode: TileMode.clamp
        )
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              size: screenAwareSize(20.0, context),

            ),
            onPressed: () {},
          ),
          title: Text("adidas",
            style: TextStyle(
              color: Colors.white,
              fontSize: screenAwareSize(18.0, context),
              fontFamily: "Montserrat-Bold"
            )
          ),
          centerTitle: true,
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.favorite_border,
                size: screenAwareSize(20.0, context),
                color: Colors.white,
              ),
              onPressed: () {},
            )
          ],
        ),
        body: Column(
          children: <Widget>[
            ProductScreenTopPart(),
            ProductScreenBottomPart()
          ],
        )
      ),
    );
  }
}